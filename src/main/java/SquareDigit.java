import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SquareDigit {

    public int squareDigits(int n) {
        return Integer.valueOf(Stream.of(Integer.toString(n).split(""))
                .mapToInt(Integer::valueOf)
                .map(i -> i * i)
                .boxed()
                .map(i -> Integer.toString(i))
                .collect(Collectors.joining()));
    }

}